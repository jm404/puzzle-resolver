// 
// Puzzle Resolver
// The purpose of this program is to solve puzzles based on the input from text file
// First row of the file represents the number of rows and columns ex: 2 2 (2x2)
// Piece is described based on his type of sides ex: 1 3 4 5 (described clockwise beginning from top)
// Output: the display of the pieces represented. Ex: 3 4 2
//                                                    5 1 7
//                                                    6 8 9  (Numbers refers to piece position in original txt)
// ****************ToDos****************
// - Improve efficience by handling corners and borders
// - Fix bugs of generating candidates
// - Fix bugs of iterating through the matrix
// - Improve efficience by handling corners and borders
//
// ****************Tests****************
//
// - Input: OK
// - Rotate piece: OK
// - Check Compatibility: OK
// - Generate Candidates : Corrections needed
// - Fill cells : Corrections needed
//

// ****************Author****************
// Jose Miguel Garcia Ramos (23-may-2018)

// ****************License****************
// MIT License
// https://opensource.org/licenses/MIT
//

#define PATH "/home/jose/Documents/Reto_Puzzle/2x2.txt"


#include <algorithm>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <fstream>
#include <string>


using namespace std;


// Rotate a piece of the puzzle one time, ex: 0123 -> 1230

string rotatePiece(string piece){

//Backup of elements

    string element = piece;
    char tmp01 = element.at(0);
    char tmp02 = element.at(1);
    char tmp03 = element.at(2);
    char tmp04 = element.at(3);

//1st rotation

    element = {tmp02,tmp03,tmp04,tmp01};

return element;

}

// Method that checks if a piece is compatible with the pieces around. Compare chars to compare sides.

bool checkCompatibility(string piece, string leftElement,string topElement, string rightElement,string botElement){

bool left = false, top = false, right = false, bot = false;

// Checking left compatibility

    if (piece[0] == leftElement[3] || leftElement == "X"){

        left = true;
    }

// Checking top compatibility

    if(piece[1] == topElement[3] || topElement == "X"){

        top = true;
    }

// Checking right compatibility

    if (piece[2] == rightElement[0] || rightElement == "X"){

        right = true;
    }

// Checking bot compatibility

    if (piece[3] == botElement[1] || botElement == "X"){

        bot = true;
    }

// Checking ALL sides

    if (left && top && right && bot){

        return true;

    }else{

        return false;
    }

}

// Method to generate a vector of strings which contains the rotations of the pieces that can fit in that place

vector<string>generateCandidates(int row, int column,int nRows, vector<vector<vector<string>>> candidates, vector<vector<string>> matrix, vector<string> candidatos){

vector<string> result;
string piece,pieceModified;

string leftElement,topElement,rightElement,botElement;

for (int i=0;i<candidatos.size();i++) {// Repeat for every piece

    piece = candidatos[i];

    for (int j=0;j<4;j++){   // Repeat for every rotation of the piece = 4 rotations

        // Handling exceptions to avoid getting out of matrix

        if (row == 0 && column == 0){               // First element

            leftElement = "0000";
            topElement= "0000";
            rightElement = matrix[row][column+1];
            botElement = matrix[row+1][column];

        }else if (row == nRows-1 && column == nRows-1){ // Last element

            leftElement = matrix[row][column-1];
            topElement = matrix[row-1][column];
            rightElement = "0000";
            botElement = "0000";

        }else if (row == 0 && column == nRows-1){

            leftElement = matrix[row][column-1];
            topElement = "0000";
            rightElement = "0000";
            botElement = matrix[row+1][column];


        }else if(row == nRows && column == 0) {

            leftElement = "0000";
            topElement = matrix[row-1][column];
            rightElement = rightElement = matrix[row][column+1];
            botElement = "0000";

        }else if (row == 0){                        // First Row

            leftElement = matrix[row][column-1];
            topElement = "0000";
            rightElement = matrix[row][column+1];
            botElement = matrix[row+1][column];

        }else if (row == nRows-1){

            leftElement = "0000";
            topElement = matrix[row-1][column];
            rightElement = matrix[row][column+1];
            botElement = "0000";


        }else if (column == 0){                     // First Column

            leftElement = "0000";
            topElement = matrix[row-1][column];
            rightElement = matrix[row][column+1];
            botElement = matrix[row+1][column];

        }else if (column == nRows-1){                 // Last Column

            leftElement = matrix[row][column-1];
            topElement = matrix[row-1][column];
            rightElement = "0000";
            botElement = matrix[row+1][column];

        }else{                                      // General case

            leftElement = matrix[row][column-1];
            topElement = matrix[row-1][column];
            rightElement = matrix[row][column+1];
            botElement = matrix[row+1][column];

        }

        bool fits = checkCompatibility(piece,leftElement,topElement,rightElement,botElement);

        // Save the candidate
        if (fits){
            result.push_back(piece);

            fits = false;
        }

        // Rotate piece

        piece = rotatePiece(piece);

        }
    }
    return result;
}


int main()
{
 	// Reading input file from local directory
 	vector<string> candidatos;

    std::ifstream file(PATH);
    std::string str;
    vector<vector<string>> pieces;

    while (std::getline(file, str))
    {
        str.erase(remove(str.begin(), str.end(), ' '), str.end());
    	candidatos.push_back(str);

    }

    for (int i=0;i<<candidatos.size();i++){
        cout << candidatos.at(i);
    }

    // Obtaining number of rows and columns

    string nRowsGetter = candidatos.at(0);
    char nRowsGetterChar = nRowsGetter[0];
    int nRows = nRowsGetterChar - '0';
    int nColumns = nRows;
    int nPieces = nRows*nColumns;
    candidatos.erase(candidatos.begin());

// Initializing vectors we will use later

    int i=0,j=0;
    vector<vector<vector<string>>> candidates(nRows,vector<vector<string>>(nRows*nColumns));
    vector<vector<string>> matrix(nRows,vector<string>(nColumns));
    vector<string> candidatesTmp(nRows*nColumns);


// Filling matrix with values "x" so i know its a not visited cell

    for (int i=0;i<nRows;i++){
        for(int j=0;j<nColumns;j++){
            matrix[i][j] = "X";
        }
    }

//Calculating solution

    while (i < nRows && j < nColumns){

        if (matrix[i][j] == "X"){ // First time i visit that cell

            candidatesTmp = generateCandidates(i,j,nRows,candidates,matrix,candidatos);
            candidates[i][j] = candidatesTmp;
            for (int i=0;i<candidatesTmp.size();i++){
            cout << candidatesTmp[i];
            }
            cout << "-";

            if (candidatesTmp.empty()){

                matrix[i][j] = "X";

                if (j = 0){
                    j=nColumns;
                    i--;
                }else {
                    j--;
                }

            }else{

                candidatesTmp = candidates[i][j];
                matrix[i][j] = candidatesTmp.back();
                candidatesTmp.erase(candidatesTmp.end());
                candidates[i][j] = candidatesTmp; // I always get the first one so i remove him

                if (j == nRows-1){
                    i++;
                    j=0;
                }else{
                    j++;
                }
              }

        }else if (generateCandidates(i,j,nRows,candidates,matrix,candidatos).empty()){

             if (j == 0){
                    j=nColumns-1;
                    i--;
                }else {
                    i--;
                }


        }else if (!generateCandidates(i,j,nRows,candidates,matrix,candidatos).empty()){

            // Assing value to cell and remove from candidates
                candidatesTmp = candidates[i][j];
                matrix[i][j] = candidatesTmp.back();
                candidatesTmp.erase(candidatesTmp.end());
                candidates[i][j] = candidatesTmp; // I always get the first one so i remove him

                if (j == nRows-1){
                    i++;
                    j=0;
                }else{
                    j++;
                }

        }else{

            cout << "ERROR";
        }

    }


    // Print solution

    for (int i=0;i<matrix.size();i++){

        for (int j=0;j<matrix.size();j++){

            cout << matrix[i][j];

        }

    }



}


