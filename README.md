# Puzzle Resolver

Aplicación que permite resolver un puzzle de tamaño nxm. Las piezas del puzzle vienen indicadas de la siguiente manera:

Existirán 4 lados distintos, cada uno designado por un numero del 0 al 4  siendo el 0 el número que designa un borde exterior.

De manera que la pieza quedará indicada como:

	3,2,1,4

El programa permite, dado un archivo txt en el que se indique en primera linea el tamaño (por ejemplo 8x8) y en cada línea una pieza separada por comas, resolver el puzzle.

# TODO

Especificado en el archivo README.txt

# Autor
Jose Miguel García Ramos. 2018

# Licencia
MIT LICENSE
